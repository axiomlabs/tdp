#ifndef GAME_H_
#define GAME_H_

#include <vector>

#include "config.h"
#include "entity.h"
#include "graphics.h"

class Map;

class Game {
    int sockfd_;
    bool connected_ = false;
    const Config config_;
    std::vector<Entity*> entities_;
    Map* map_;
    struct addrinfo *res_;

    int eventLoop();
    void update(int delta_time_ms);
    void draw(Graphics& graphics);
    int connect(std::string,std::string);

public:
    Game(Config& config);
    ~Game();
};

#endif
