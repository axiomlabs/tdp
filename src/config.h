#ifndef CONFIG_H_
#define CONFIG_H_

#include <string>
#include <unordered_map>

#include "arguments.h"

class Config {
    std::unordered_map<std::string, std::string> options_;
    std::string trimString(const std::string in) const;

    void makeConfig(std::string filename) const;

public:
    Config(const Arguments& args);
    std::string getValue(const std::string& key) const;
    std::string getValue(const std::string& key, const std::string& default_value) const;

    int getInt(const std::string& key) const;
    int getInt(const std::string& key, const int& default_value) const;

    float getFloat(const std::string& key) const;
    float getFloat(const std::string& key, const float& default_value) const;
};

#endif
