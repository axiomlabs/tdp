#ifndef KINEMATICS_H_
#define KINEMATICS_H_

class Kinematics {
public:
    inline Kinematics(float pos, float vel);

    float pos;
    float vel;

    inline void update(int delta_time_ms);
};

#endif

Kinematics::Kinematics(float position, float velocity) :
    pos(position), vel(velocity) {}

void Kinematics::update(int delta_time_ms) {
    pos += vel * delta_time_ms;
}
