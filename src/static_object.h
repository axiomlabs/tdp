#ifndef STATIC_OBJECT_H_
#define STATIC_OBJECT_H_

class Graphics;
class Sprite;
struct SDL_Rect;

class StaticObject {
    Sprite* sprite_;
    int x_, y_, width_, height_;

public:
    StaticObject(Graphics& graphics,
        int x, int y, int width, int height,
        int sprite_x, int sprite_y);
    ~StaticObject();

    void draw(Graphics& graphics) const;

    SDL_Rect getCollider() const;
};

#endif
