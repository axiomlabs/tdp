#ifndef SPRITE_H_
#define SPRITE_H_

#include <string>

struct SDL_Texture;
class Graphics;

class Sprite {
    const int x_, y_, width_, height_;
    SDL_Texture* sprite_sheet_;
  public:
    Sprite(
        Graphics& graphics,
        std::string sprite_sheet_path,
        int x, int y,
        int width, int height);
    void draw(Graphics& graphics, int x, int y);
};

#endif
