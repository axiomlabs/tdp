#include "static_object.h"

#include <SDL2/SDL.h>

#include "graphics.h"
#include "sprite.h"

StaticObject::StaticObject(
    Graphics& graphics,
    int x, int y,
    int width, int height,
    int sprite_x, int sprite_y) :
        sprite_(new Sprite(graphics, "test", sprite_x, sprite_y, width, height)),
        x_(x), y_(y), width_(width), height_(height) {}

StaticObject::~StaticObject() {
    delete sprite_;
}

void StaticObject::draw(Graphics& graphics) const {
    sprite_->draw(graphics, x_, y_);
}

SDL_Rect StaticObject::getCollider() const {
    SDL_Rect tmp = {x_, y_, width_, height_};
    return tmp;
}
