#include "server.h"

#include <arpa/inet.h>
#include <cstdio>
#include <cstring>
#include <sys/socket.h>
#include <unistd.h>

Server::Server()
: sockfd_(socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))
{}

Server::~Server() {
}

int Server::setup(){
    struct sockaddr_in serveraddr = { 0 };
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serveraddr.sin_port = htons(14321);

    struct sockaddr_in clientaddr;

    char buf[200];

    if (bind(sockfd_, (struct sockaddr *)&serveraddr, sizeof(struct sockaddr_in)) < 0) {
        perror("bind()");
        return -1;
    }

    socklen_t clientaddrlen = sizeof(struct sockaddr_in);

    while (1) {
        printf("pls data\n");
        fflush(stdout);

        const ssize_t recv_len = recvfrom(sockfd_, buf, 200, 0,
                                          (struct sockaddr *)&clientaddr,
                                          &clientaddrlen);
        if (recv_len == -1) {
            perror("recvfrom()");
            return -1;
        }

        if (strcmp(buf, "end") == 0) {
            break;
        }

        printf("Data from %s:%d\n", inet_ntoa(clientaddr.sin_addr), ntohs(clientaddr.sin_port));
        printf("%s\n", buf);

        const ssize_t sent_len = sendto(sockfd_, buf, recv_len, 0,
                                        (struct sockaddr *)&clientaddr,
                                        clientaddrlen);
        if (sent_len == -1) {
            perror("sendto()");
            return -1;
        }
    }

    return 0;
}
