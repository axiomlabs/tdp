#ifndef SERVER_H_
#define SERVER_H_

class Server {
    int sockfd_;

public:
    Server();
    ~Server();

    int setup();
};

#endif
