#include "config.h"

#include <algorithm>
#include <fstream>
#include <sstream>
#include <iostream>

std::string Config::trimString(const std::string in) const {
    std::string out = in;
    out.erase(
            std::remove_if(out.begin(), out.end(),
                [](char c)
                {
                return std::isspace<char>(c, std::locale::classic());
                }),
            out.end());

    return out;
}

void Config::makeConfig(std::string filename) const {
    std::ofstream file;
    file.open(filename);
    file << "[General]\n";
    file << "[Controls]\n";
    file << "p1.up=w\n" << "p1.down=s\n" << "p1.left=a\n" << "p1.right=d\n";
    file << "p2.up=Up\n" << "p2.down=Down\n" <<"p2.left=Left\n" << "p2.right=Right";
    file.close();
}

Config::Config(const Arguments& args) {
    std::ifstream file;
    std::string line;

    std::string filename = args.getValue("config", "tdp.ini");

    file.open(filename);
    if (!file.is_open()) {
        std::cout << "Config file not found! Creating...\n";
        makeConfig(filename);
        file.open(filename);
    }

    options_ = args.getMap();

    while (!file.eof()) {
        std::getline(file, line);
        trimString(line);

        if (line.empty() || line.at(0) == '[' || line.at(0) == ';') {
            continue;
        }

        std::string arg, param;
        std::stringstream entire_line(line);
        std::getline(entire_line, arg, '=');
        std::getline(entire_line, param);

        if (param.empty()) continue;

        param = args.getValue(arg, param);
        options_.emplace(arg, param);
        std::cout << "Loaded option : " << arg << " = "
            << param << std::endl;
    }

    std::printf("Config loaded with %d options \n", int(options_.size()));
}

std::string Config::getValue(const std::string& key) const {
    auto iter = options_.find(key);
    return iter == options_.end() ? "" : iter->second;
}

std::string Config::getValue(const std::string& key, const std::string& default_value) const {
    std::string value = getValue(key);
    return value.empty() ? default_value : value;
}

int Config::getInt(const std::string& key) const {
    return std::stoi(getValue(key));
}

int Config::getInt(const std::string& key, const int& default_value) const {
    std::string value = getValue(key);
    return value.empty() ? default_value : std::stoi(value);
}

float Config::getFloat(const std::string& key) const {
    return std::stof(getValue(key));
}

float Config::getFloat(const std::string& key, const float& default_value) const {
    std::string value = getValue(key);
    return value.empty() ? default_value : std::stof(value);
}
