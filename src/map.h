#ifndef MAP_H_
#define MAP_H_

#include <vector>

class Graphics;
struct SDL_Rect;
class StaticObject;


class Map {
    std::vector<StaticObject*> static_objects_;
    int width_, height_;

public:
    void draw(Graphics& graphics) const;
    std::vector<StaticObject*> getCollidingObjects(const SDL_Rect& other) const;
    static Map* createTestMap(Graphics& graphics);
};

#endif
