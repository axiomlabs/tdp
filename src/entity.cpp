#include "entity.h"

#include <math.h>

#include "graphics.h"
#include "sprite.h"

Entity::Entity(Graphics& graphics,
               float x, float y,
               int width, int height,
               std::string sprite_sheet,
               int sprite_x, int sprite_y,
               int sprite_width, int sprite_height,
               float max_velocity,
               float acceleration, float friction)
: kWidth(width), kHeight(height)
, kAcceleration(acceleration), kFriction(friction)
, kMaxVelocity(max_velocity)
, collider_({ int(kinematics_x_.pos), int(kinematics_y_.pos)
            , kWidth, kHeight })
, kinematics_x_(x, 0.f), kinematics_y_(y, 0.f)
, sprite_(new Sprite(graphics, sprite_sheet
, sprite_x, sprite_y, sprite_width, sprite_height))
, acceleration_x_(0.f), acceleration_y_(0.f)
{}

void Entity::startMoving(Directions::Direction direction) {
    switch (direction) {
        case Directions::LEFT:
            acceleration_x_ -= kAcceleration;
            break;
        case Directions::UP:
            acceleration_y_ -= kAcceleration;
            break;
        case Directions::DOWN:
            acceleration_y_ += kAcceleration;
            break;
        case Directions::RIGHT:
            acceleration_x_ += kAcceleration;
            break;
    }
}

void Entity::stopMoving(Directions::Direction direction) {
    if (direction == Directions::UP || direction == Directions::DOWN) {
        acceleration_y_ = 0;
    } else {
        acceleration_x_ = 0;
    }
}

void Entity::update(const int delta_time_ms, const SDL_Rect& collider) {
    update_kinematics(delta_time_ms,
        kinematics_x_, acceleration_x_, collider, Axis::X);
    update_kinematics(delta_time_ms,
        kinematics_y_, acceleration_y_, collider, Axis::Y);
}

void Entity::update_kinematics(const int delta_time_ms,
                               Kinematics& kinematics, const float acceleration,
                               const SDL_Rect& collider, const Axis::Axis axis) {
    if (kinematics.vel > 0) {
        kinematics.vel = kinematics.vel - kFriction < 0 ? 0 : kinematics.vel - kFriction;
    } else if (kinematics.vel < 0) {
        kinematics.vel = kinematics.vel + kFriction > 0 ? 0 : kinematics.vel + kFriction;
    }

    if (acceleration > 0) {
        kinematics.vel = kinematics.vel + acceleration >= kMaxVelocity ?
            kMaxVelocity : kinematics.vel + acceleration;
    } else if (acceleration < 0) {
        kinematics.vel = kinematics.vel + acceleration <= -(kMaxVelocity) ?
            -(kMaxVelocity) : kinematics.vel + acceleration;
    }

    if (kinematics.vel == 0) {
        return;
    }

    kinematics.update(delta_time_ms);

    if (kinematics.pos < 0) {
        kinematics.pos = 0;
    }

    if (axis == Axis::X) {
        if (kinematics.pos + kWidth > kScreenWidth) {
            kinematics.pos = float(kScreenWidth - kWidth);
        }

        collider_.x = int(kinematics.pos);
        if (collidedWith(collider)) {
            kinematics.pos =
                float(kinematics.pos - collisionDelta(collider, Axis::X));
            collider_.x = int(kinematics.pos);
        }
    } else {
        if (kinematics.pos + kHeight > kScreenHeight) {
            kinematics.pos = float(kScreenHeight - kHeight);
        }

        collider_.y = int(kinematics.pos);
        if (collidedWith(collider)) {
            kinematics.pos =
                float(kinematics.pos - collisionDelta(collider, Axis::Y));
            collider_.y = int(kinematics.pos);
        }
    }
}

void Entity::draw(Graphics& graphics) {
    sprite_->draw(graphics, int(kinematics_x_.pos), int(kinematics_y_.pos));
}

bool Entity::collidedWith(const SDL_Rect& other) {
    return SDL_HasIntersection(&collider_, &other) == SDL_TRUE;
}

int Entity::collisionDelta(const SDL_Rect& other, const Axis::Axis axis) {
    if (axis == Axis::X) {
        if (kinematics_x_.vel >= 0) {
            return collider_.x + collider_.w - other.x;
        } else {
            return -(other.x + other.w - collider_.x);
        }
    } else {
        if (kinematics_y_.vel >= 0) {
            return collider_.y + collider_.h - other.y;
        } else {
            return -(other.y + other.h - collider_.y);
        }
    }
}
