#include "sprite.h"

#include <SDL2/SDL.h>

#include "graphics.h"

// Load sprite_sheet_name, and set up a sprite at position x, y in the sprite sheet.
Sprite::Sprite(
    Graphics& graphics,
    std::string sprite_sheet_path,
    int x, int y,
    int width, int height) :
        x_(x), y_(y),
        width_(width), height_(height),
        sprite_sheet_(graphics.loadImage(sprite_sheet_path)) {
    printf("Initialising sprite: %s\nx: %d; y: %d;\n", sprite_sheet_path.c_str(), x, y);
    printf("width: %d; height: %d;\n", width, height);
    for (int i = 0; i < 80; i++) {
        printf("-");
    }
    printf("\n");
}


// x and y are where on the screen you want the sprite to be drawn.
void Sprite::draw(Graphics& graphics, int x, int y) {
    const SDL_Rect srcRect = {x_, y_, width_, height_};
    const SDL_Rect destRect = {x, y, width_, height_};
    graphics.renderTexture(sprite_sheet_, &srcRect, &destRect);
}
