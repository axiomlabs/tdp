#include "enemy.h"

const int kWidth = 32;
const int kHeight = 32;

const int kSpriteWidth = 32;
const int kSpriteHeight = 32;

const float kAcceleration = .1f;
const float kFriction = .04f;
const float kMaxVelocity = .35f;

Enemy::Enemy(Graphics& graphics,
        float x, float y,
        std::string sprite_sheet,
        int sprite_x, int sprite_y) :
    Entity(graphics,
            x, y,
            ::kWidth, ::kHeight,
            sprite_sheet,
            sprite_x, sprite_y,
            ::kSpriteWidth, ::kSpriteHeight,
            ::kMaxVelocity,
            ::kAcceleration, ::kFriction) {}
