#include "game.h"

#include <arpa/inet.h>
#include <netdb.h>
#include <SDL2/SDL.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "controls.h"
#include "graphics.h"
#include "input.h"
#include "map.h"
#include "player.h"
#include "static_object.h"

const int kFps = 60;
const int kMaxFrameTime = 5 * 1000 / 60;

int max(int a, int b) { return (a > b) ? a : b; }
int min(int a, int b) { return (max(a,b) == a) ? b : a; }

Game::Game(Config& config) :
    config_(config)
{
    SDL_Init(SDL_INIT_VIDEO);
    eventLoop();
}

Game::~Game() {
    SDL_Quit();
}

int Game::eventLoop() {
    Graphics graphics;
    Input input;
    SDL_Event event;

    map_ = Map::createTestMap(graphics);

    if (config_.getInt("online", 0) == 1
        && connect(config_.getValue("host", "127.0.0.1"),
                   config_.getValue("port", "14321")) == 0)
    {
        connected_ = true;
    }

    Controls p1_controls(config_.getValue("p1.left", "a"),
                         config_.getValue("p1.up", "w"),
                         config_.getValue("p1.down", "s"),
                         config_.getValue("p1.right", "d"));

    Controls p2_controls(config_.getValue("p2.left", "Left"),
                         config_.getValue("p2.up", "Up"),
                         config_.getValue("p2.down", "Down"),
                         config_.getValue("p2.right", "Right"));

    entities_.push_back(static_cast<Entity *>(new Player(graphics,
                                                         kScreenWidth / 2,
                                                         kScreenHeight / 4,
                                                         "test",  0, 0,
                                                         p1_controls)));
    entities_.push_back(static_cast<Entity *>(new Player(graphics,
                                                         kScreenWidth / 4,
                                                         kScreenHeight / 2,
                                                         "test", 32, 0,
                                                         p2_controls)));

    bool running = true;
    int last_update_time = SDL_GetTicks();

    while (running) {
        if (connected_) {
            char buf[7] = "blabla";
            if (sendto(sockfd_, buf, 7, 0, res_->ai_addr, res_->ai_addrlen) == -1) {
                perror("sendto()");
            }

            memset(buf, '\0', 7);

            if (recvfrom(sockfd_, buf, 7, 0, res_->ai_addr, &res_->ai_addrlen) == -1) {
                perror("recvfrom()");
            }

            printf("%s", buf);
            fflush(stdout);
        }

        const int start_time_ms = SDL_GetTicks();
        while (SDL_PollEvent(&event)) {
            switch (event.key.type) {
                case SDL_KEYDOWN:
                    input.keyDownEvent(event);
                    break;

                case SDL_KEYUP:
                    input.keyUpEvent(event);
                    break;

                default:
                    break;
            }

            if (input.wasKeyPressed(SDL_SCANCODE_ESCAPE)) {
                running = false;
            }

            for (size_t i = 0; i < entities_.size(); i++) {
                Player *tmp = dynamic_cast<Player *>(entities_[i]);
                if (tmp) {
                    tmp->handleInput(input);
                }
            }
        }

        const int delta_time_ms = SDL_GetTicks() - last_update_time;
        update(min(delta_time_ms, kMaxFrameTime));
        last_update_time = SDL_GetTicks();

        draw(graphics);

        const int time_per_frame_ms = 1000 / kFps;
        const int elapsed_time_ms = SDL_GetTicks() - start_time_ms;
        if (elapsed_time_ms < time_per_frame_ms) {
            SDL_Delay(time_per_frame_ms - elapsed_time_ms);
        }
    }

    if (connected_) {
        char buf[4] = "end";
        sendto(sockfd_, buf, 4, 0, res_->ai_addr, res_->ai_addrlen);
        close(sockfd_);
        freeaddrinfo(res_);
    }

    return 0;
}

void Game::update(int delta_time_ms) {
    // Store all SDL_Rects and check collision against all of them in one update.

    for (size_t i = 0; i < entities_.size(); i++) {
        for (size_t j = 0; j < entities_.size(); j++) {
            if (entities_[i] != entities_[j]) {
                entities_[i]->update(delta_time_ms, entities_[j]->getCollider());
            }
        }
        std::vector<StaticObject*> map_objects = map_->getCollidingObjects(entities_[i]->getCollider());
        for (size_t j = 0; j < map_objects.size(); j++) {
            entities_[i]->update(delta_time_ms, map_objects[j]->getCollider());
        }
    }
}

void Game::draw(Graphics& graphics) {
    graphics.clear();

    {
        static int x = 0;
        SDL_Rect srcRect = {0, 0, 64, 64};
        SDL_Rect destRect = {x++, 480 / 2, 64, 64};

        SDL_Texture *test = graphics.loadImage("test");
        graphics.renderTexture(test, &srcRect, &destRect);
        x > 640 + 64 ? x = -64 : 0;
    }

    for (size_t i = 0; i < entities_.size(); i++) {
        entities_[i]->draw(graphics);
    }

    map_->draw(graphics);

    graphics.flip();
}

int Game::connect(std::string host,std::string port) {
    struct addrinfo hints;

    memset(&hints,0,sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_ADDRCONFIG;

    int err = getaddrinfo(host.c_str(), port.c_str(), &hints, &res_);
    if (err != 0) {
        gai_strerror(err);
        return -1;
    }

    sockfd_ = socket(res_->ai_family,res_->ai_socktype,res_->ai_protocol);
    if(sockfd_ < 0) {
        perror("socket()");
        return -1;
    }

    return 0;
}
