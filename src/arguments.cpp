#include "arguments.h"

#include <iostream>
#include <getopt.h>

void Arguments::parseArgs(int argc, char** argv) {
    char c;
    while ((c = getopt(argc, argv, optstring_)) != -1) {
        switch (c) {
            case 'f':
                options_.emplace("config",std::string(optarg));
                break;
            case 'o':
                options_.emplace("online","1");
                options_.emplace("host",std::string(optarg));
                break;
            case 'p':
                options_.emplace("port",std::string(optarg));
                break;
            default:
                break;
        }
    }
}

Arguments::Arguments(int argc, char** argv) {
    optstring_ = "s:f:vo:p:";
    parseArgs(argc, argv);
}

std::string Arguments::getValue(std::string key) const {
    auto iter = options_.find(key);
    return iter == options_.end() ? "" : iter->second;
}

std::string Arguments::getValue(std::string key, std::string default_val) const {
    std::string val = getValue(key);
    return val.empty() ? default_val : val;
}

std::unordered_map<std::string, std::string> Arguments::getMap() const {
    return options_;
}
