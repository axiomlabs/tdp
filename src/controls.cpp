#include "controls.h"

#include <SDL2/SDL.h>
#include <string>
#include <unordered_map>

Controls::Controls() {
    setKey("W", Actions::UP);
    setKey("S", Actions::DOWN);
    setKey("A", Actions::LEFT);
    setKey("D", Actions::RIGHT);
}

Controls::Controls(std::string left, std::string up,
                   std::string down, std::string right)
{
    setKey(up, Actions::UP);
    setKey(down, Actions::DOWN);
    setKey(left, Actions::LEFT);
    setKey(right, Actions::RIGHT);
}

void Controls::setKey(const std::string& key, const Actions::Action& action) {
    if (keymap.count(action) > 0) {
        printf("Writing over previous keybind: %s\n",
            SDL_GetScancodeName(keymap[action]));
        printf("Replacing with: %s\n", key.c_str());
    }

    keymap[action] = SDL_GetScancodeFromName(key.c_str());
}

SDL_Scancode Controls::getKey(const Actions::Action& action) {
    return keymap[action];
}
