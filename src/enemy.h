#ifndef ENEMY_H_
#define ENEMY_H_

#include "entity.h"

class Graphics;

class Enemy: public Entity {
    const int kWidth = 32;
    const int kHeight = 32;
    const int kSpriteWidth = 32;
    const int kSpriteHeight = 32;

    const float kAcceleration = .1f;
    const float kFriction = .04f;
    const float kMaxVelocity = .35f;

public:
    Enemy(Graphics& graphics,
        float x, float y,
        std::string sprite_sheet,
        int sprite_x, int sprite_y);
};

#endif
