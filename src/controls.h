#ifndef CONTROLS_H_
#define CONTROLS_H_

#include <SDL2/SDL.h>
#include <unordered_map>

namespace Actions {
    typedef int Action;
    enum { LEFT, UP, DOWN, RIGHT };
}

class Controls {
    std::unordered_map<Actions::Action, SDL_Scancode> keymap;

public:
    Controls();
    Controls(std::string left, std::string up,
             std::string down, std::string right);
    void setKey(const std::string& key, const Actions::Action& action);
    SDL_Scancode getKey(const Actions::Action& action);
};

#endif
