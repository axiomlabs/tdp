#ifndef GRAPHICS_H_
#define GRAPHICS_H_

#include <map>
#include <string>

// Temporary values
const int kScreenWidth = 640;
const int kScreenHeight = 480;

struct SDL_Texture;
struct SDL_Window;
struct SDL_Rect;
struct SDL_Renderer;

class Graphics {
    SDL_Window* window_;
    SDL_Renderer* renderer_;
    std::map<std::string, SDL_Texture*> sprite_sheet_;

public:
    Graphics();
    ~Graphics();

    void renderTexture(
        SDL_Texture* source,
        const SDL_Rect* source_rectangle,
        const SDL_Rect* destination_rectangle) const;
    SDL_Texture* loadImage(const std::string& filename);
    void clear() const;
    void flip() const;
};

#endif
