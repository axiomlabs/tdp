#ifndef ENTITY_H_
#define ENTITY_H_

#include <SDL2/SDL.h>
#include <string>

#include "kinematics.h"

class Graphics;
class Sprite;

namespace Axis {
    enum Axis { X, Y };
}

namespace Directions {
    enum Direction { LEFT, UP, DOWN, RIGHT };
}

class Entity {
private:
    const int kWidth;
    const int kHeight;

    const float kAcceleration;
    const float kFriction;
    const float kMaxVelocity;

    SDL_Rect collider_;
    Kinematics kinematics_x_, kinematics_y_;
    Sprite* sprite_;

    void update_kinematics(const int delta_time_ms,
                           Kinematics& kinematics, const float acceleration,
                           const SDL_Rect& collider, const Axis::Axis axis);

    bool collidedWith(const SDL_Rect& other);
    int collisionDelta(const SDL_Rect& other, const Axis::Axis axis);

protected:
    // Used to determine previous heading in subclasses.
    float acceleration_x_, acceleration_y_;
    void startMoving(Directions::Direction direction);
    void stopMoving(Directions::Direction direction);

public:
    Entity(Graphics& graphics,
           float x, float y,
           int width, int height,
           std::string sprite_sheet,
           int sprite_x, int sprite_y,
           int sprite_width, int sprite_height,
           float max_velocity,
           float acceleration, float friction);
    virtual ~Entity();

    void update(const int delta_time_ms, const SDL_Rect& collider);
    void draw(Graphics& graphics);

    SDL_Rect getCollider() const { return collider_; };
};

#endif
