#include "graphics.h"

#include <SDL2/SDL.h>



Graphics::Graphics() {
    window_ = SDL_CreateWindow(
            "The game",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            kScreenWidth,
            kScreenHeight,
            SDL_WINDOW_RESIZABLE);
    renderer_ = SDL_CreateRenderer(window_, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest");
    SDL_RenderSetLogicalSize(renderer_, kScreenWidth, kScreenHeight);
    SDL_ShowCursor(SDL_DISABLE);
}

Graphics::~Graphics() {
    SDL_DestroyRenderer(renderer_);
    SDL_DestroyWindow(window_);
}

SDL_Texture* Graphics::loadImage(const std::string& filename) {
    if (sprite_sheet_.count(filename) == 0) {
        const std::string filepath = "res/sprites/" + filename + ".bmp";
        printf("Loading texture %s\n", filepath.c_str());
        SDL_Surface* surface = SDL_LoadBMP(filepath.c_str());
        sprite_sheet_[filename] = SDL_CreateTextureFromSurface(renderer_, surface);
        SDL_FreeSurface(surface);
        if (sprite_sheet_[filename] == NULL) {
            printf("Texture %s couldn't be loaded.\n", filepath.c_str());
            return NULL;
        }
    }
    return sprite_sheet_[filename];
}

void Graphics::renderTexture(
        SDL_Texture* source,
        const SDL_Rect* source_rectangle,
        const SDL_Rect* destination_rectangle) const {
    SDL_RenderCopy(renderer_, source, source_rectangle, destination_rectangle);
}

void Graphics::clear() const {
    SDL_RenderClear(renderer_);
}

void Graphics::flip() const {
    SDL_RenderPresent(renderer_);
}
