#include "game.h"
#include "config.h"

void init(const Arguments& args) {
    Config config(args);
    Game game(config);
}

int main(int argc, char** argv) {
    Arguments cmd_args(argc, argv);

    init(cmd_args);
    return 0;
}
