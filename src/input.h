#ifndef INPUT_H_
#define INPUT_H_

#include <map>
#include <SDL2/SDL.h>

class Input {
    std::map<SDL_Scancode, bool> held_keys_;
    std::map<SDL_Scancode, bool> pressed_keys_;
    std::map<SDL_Scancode, bool> released_keys_;
public:
    void beginNewFrame();

    void keyDownEvent(const SDL_Event& event);
    void keyUpEvent(const SDL_Event& event);

    bool wasKeyPressed(const SDL_Scancode key);
    bool wasKeyReleased(const SDL_Scancode key);

    bool isKeyHeld(const SDL_Scancode key);
};

#endif
