#ifndef ARGUMENTS_H_
#define ARGUMENTS_H_

#include <string>
#include <unordered_map>
#include <unordered_set>

class Arguments {
    const char* optstring_;
    std::unordered_map<std::string, std::string> options_;

    void parseArgs(int argc, char** argv);

public:
    Arguments(int argc, char** argv);

    std::string getValue(std::string key) const;
    std::string getValue(std::string key, std::string default_value) const;

    std::unordered_map<std::string, std::string> getMap() const;
};

#endif
