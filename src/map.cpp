#include "map.h"

#include <SDL2/SDL.h>

#include "static_object.h"

void Map::draw(Graphics& graphics) const {
    for (auto it = static_objects_.begin(); it != static_objects_.end(); it++) {
        (*it)->draw(graphics);
    }
}

std::vector<StaticObject*> Map::getCollidingObjects(const SDL_Rect& other) const {
    std::vector<StaticObject*> tmp;

    for (size_t i = 0; i < static_objects_.size(); i++) {
        SDL_Rect asdf = static_objects_[i]->getCollider();
        if (SDL_HasIntersection(&other, &asdf) == SDL_TRUE) {
            tmp.push_back(static_objects_[i]);
        }
    }

    return tmp;
}

// static
Map* Map::createTestMap(Graphics& graphics) {
    Map* map = new Map();
    map->static_objects_.push_back(new StaticObject(graphics, 128, 128, 32, 32, 0, 32));
    map->static_objects_.push_back(new StaticObject(graphics, 160, 128, 32, 32, 0, 32));
    map->static_objects_.push_back(new StaticObject(graphics, 160, 96, 32, 32, 0, 32));
    map->static_objects_.push_back(new StaticObject(graphics, 192, 128, 32, 32, 0, 32));
    map->static_objects_.push_back(new StaticObject(graphics, 136, 160, 16, 16, 0, 32));
    map->static_objects_.push_back(new StaticObject(graphics, 136, 176, 14, 16, 0, 32));
    map->static_objects_.push_back(new StaticObject(graphics, 136, 192, 12, 16, 0, 32));
    map->static_objects_.push_back(new StaticObject(graphics, 420, 240, 32, 32, 0, 32));
    map->static_objects_.push_back(new StaticObject(graphics, 512, 192, 16, 32, 0, 32));
    map->static_objects_.push_back(new StaticObject(graphics, 512, 224, 16, 32, 0, 32));
    map->static_objects_.push_back(new StaticObject(graphics, 512, 256, 16, 32, 0, 32));
    return map;
}
