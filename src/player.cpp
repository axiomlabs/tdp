#include "player.h"

#include "input.h"

const int kWidth = 32;
const int kHeight = 32;

const int kSpriteWidth = 32;
const int kSpriteHeight = 32;

const float kAcceleration = .1f;
const float kFriction = .04f;
const float kMaxVelocity = .35f;

Player::Player(Graphics& graphics,
    float x, float y,
    std::string sprite_sheet,
    int sprite_x, int sprite_y,
    Controls controls) :
        Entity(graphics,
            x, y,
            ::kWidth, ::kHeight,
            sprite_sheet,
            sprite_x, sprite_y,
            ::kSpriteWidth, ::kSpriteHeight,
            ::kMaxVelocity,
            ::kAcceleration, ::kFriction),
        controls_(controls) {}

Entity::~Entity() {}

void Player::handleInput(Input& input) {
    if (input.isKeyHeld(controls_.getKey(Actions::UP))
        && input.isKeyHeld(controls_.getKey(Actions::DOWN)))
    {
        stopMoving(acceleration_y_ > 0 ? Directions::DOWN : Directions::UP);
    } else if (input.isKeyHeld(controls_.getKey(Actions::UP))) {
        startMoving(Directions::UP);
    } else if (input.isKeyHeld(controls_.getKey(Actions::DOWN))) {
        startMoving(Directions::DOWN);
    } else {
        stopMoving(acceleration_y_ > 0 ? Directions::DOWN : Directions::UP);
    }

    if (input.isKeyHeld(controls_.getKey(Actions::LEFT))
        && input.isKeyHeld(controls_.getKey(Actions::RIGHT)))
    {
        stopMoving(acceleration_x_ > 0 ? Directions::RIGHT : Directions::LEFT);
    } else if (input.isKeyHeld(controls_.getKey(Actions::LEFT))) {
        startMoving(Directions::LEFT);
    } else if (input.isKeyHeld(controls_.getKey(Actions::RIGHT))) {
        startMoving(Directions::RIGHT);
    } else {
        stopMoving(acceleration_x_ > 0 ? Directions::RIGHT : Directions::LEFT);
    }
}
