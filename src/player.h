#ifndef PLAYER_H_
#define PLAYER_H_

#include <SDL2/SDL.h>

#include "controls.h"
#include "entity.h"

class Input;

class Player: public Entity {
    Controls controls_;
public:
    Player(Graphics& graphics,
        float x, float y,
        std::string sprite_sheet,
        int sprite_x, int sprite_y,
        Controls controls);

    void handleInput(Input& input);
};

#endif
