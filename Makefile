all:
	cd src; g++ -std=c++11 -g -Wall -Wextra *.cpp -lSDL2 -o ../tdp

nowarnings:
	cd src; g++ -std=c++11 -g *.cpp -lSDL2 -o ../tdp

mac:
	cd src; g++ -std=c++11 -g -Wall -Wextra *.cpp -framework SDL2 -o ../tdp

mac-nowarnings:
	cd src; g++ -std=c++11 -g *.cpp -framework SDL2 -o ../tdp

server:
	cd src/server; g++ -std=c++11 -g -Wall -Wextra -Wno-missing-field-initializers *.cpp -o ../../server

clean:
	rm -v tdp server
